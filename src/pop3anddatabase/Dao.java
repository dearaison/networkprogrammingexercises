package pop3anddatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Dao {
	private static final Dao instance = new Dao();
	private static final String DRIVER = "sun.jdbc.odbc.JdbcOdbcDriver";

	private static final String URL = "jdbc:odbc:student";
	public static Dao getInstance() {
		return instance;
	}

	private Connection connection;

	/**
	 * @param connection
	 */
	private Dao() {
		try {
			Class.forName(DRIVER);
			this.connection = DriverManager.getConnection(URL);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean checkPass(String username, String pass) {
		try {
			PreparedStatement statement = connection
					.prepareStatement("select * from user where username = ? && password = ?");

			statement.setString(1, username.toUpperCase());
			statement.setString(2, pass);

			ResultSet resultSet = statement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean checkUser(String username) {
		try {
			PreparedStatement statement = connection.prepareStatement("select * from table user where username = ?");
			statement.setString(1, username.toUpperCase());

			ResultSet resultSet = statement.executeQuery();

			return resultSet.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<Student> selectStudent(double score) {
		List<Student> result = new ArrayList<Student>();
		try {
			PreparedStatement statement = connection.prepareStatement("select * from student where Score = ?");
			statement.setDouble(1, score);

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				result.add(new Student(resultSet.getString("ID"), resultSet.getString("Name"), resultSet.getInt("Age"),
						resultSet.getDouble("Score")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Student> selectStudent(int age) {
		List<Student> result = new ArrayList<Student>();
		try {
			PreparedStatement statement = connection.prepareStatement("select * from student where Age = ?");
			statement.setInt(1, age);

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				result.add(new Student(resultSet.getString("ID"), resultSet.getString("Name"), resultSet.getInt("Age"),
						resultSet.getDouble("Score")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Student> selectStudent(String name) {
		List<Student> result = new ArrayList<Student>();
		try {
			PreparedStatement statement = connection.prepareStatement("select * from student where name like '%?'");
			statement.setString(1, name);

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				result.add(new Student(resultSet.getString("ID"), resultSet.getString("Name"), resultSet.getInt("Age"),
						resultSet.getDouble("Score")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
