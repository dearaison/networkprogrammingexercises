package pop3anddatabase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import javax.naming.CommunicationException;

public class ServerProcessing implements Runnable {
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;

	private Dao dao;

	private String command;
	private String param;

	private String username = null;
	private boolean singed;

	public ServerProcessing(Socket socket) throws IOException {
		this.socket = socket;
		this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		this.out = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream()), true);
		this.dao = Dao.getInstance();
	}

	private void commandAnalyze(String line) throws CommunicationException {
		int commandSeparatorIndex = line.indexOf(" ");

		if (commandSeparatorIndex < 0) {
			throw new CommunicationException("Command syntax is wrong!");
		}

		command = line.substring(0, commandSeparatorIndex).toUpperCase();
		param = line.substring(commandSeparatorIndex + 1);

	}

	private String enterPass() {
		if (username == null) {
			return "Invalid sign in!";
		}

		if (!dao.checkPass(username, param)) {
			return "Wrong password!";
		}

		singed = true;
		return "Sign in successfully";
	}

	private String enterUser() {
		if (!dao.checkUser(param)) {
			return "Wrong username!";
		}

		this.username = param;
		return "Right username";
	}

	private void findByAge() throws NumberFormatException {
		sendMultiLine(dao.selectStudent(Integer.parseInt(param)));
	}

	private void findByName() {
		sendMultiLine(dao.selectStudent(param));

	}

	private void findByScore() throws NumberFormatException {
		sendMultiLine(dao.selectStudent(Double.parseDouble(param)));

	}

	@Override
	public void run() {
		out.println("Welcome...");
		String line;
		String respond;
		try {
			while (!singed) {
				try {
					line = in.readLine();
					if ("QUIT".equalsIgnoreCase(line))
						break;

					commandAnalyze(line);

					switch (command) {
					case "USER":
						respond = enterUser();
						break;

					case "PASS":
						respond = enterPass();
						break;

					default:
						respond = "Command not found!";
						break;
					}

					out.println(respond);
				} catch (CommunicationException e) {
					out.println(e.getMessage());
				} catch (IOException e) {
					out.println("Server Error!");
				}
			}

			while (singed) {
				try {
					line = in.readLine();
					if ("QUIT".equalsIgnoreCase(line))
						break;

					commandAnalyze(line);

					switch (command) {
					case "FINDBYNAME":
						findByName();
						break;

					case "FINDBYAGE":
						findByAge();
						break;

					case "FINDBYSCORE":
						findByScore();
						break;

					default:
						respond = "Command not found!";
						break;
					}

				} catch (CommunicationException e) {
					out.println(e.getMessage());
				} catch (NumberFormatException e) {
					out.println("Wrong number format!");
				}
			}
			socket.close();
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	private void sendMultiLine(List<Student> students) {
		StringBuffer buffer = new StringBuffer();
		for (Student student : students) {
			buffer.append(student.toString() + System.lineSeparator());
		}
		out.println(buffer.toString());
	}

}
