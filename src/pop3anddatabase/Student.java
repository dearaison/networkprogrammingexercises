package pop3anddatabase;

public class Student {
	private String id;
	private String name;
	private int age;
	private double score;

	/**
	 * @param id
	 * @param name
	 * @param telphone
	 * @param score
	 */
	public Student(String id, String name, int age, double score) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.score = score;
	}

	@Override
	public String toString() {
		return id + "\t" + name + "\t" + age + "\t" + score + "\t";
	}
}
