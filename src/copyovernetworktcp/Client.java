package copyovernetworktcp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;

import javax.naming.CommunicationException;

public class Client {
	public static void main(String[] args) throws IOException {
		new Client();
	}
	private DataInputStream netIn;

	private DataOutputStream netOut;

	private Client() throws IOException {
		super();
		Socket socket = new Socket("localhost", 2000);
		netIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		netOut = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));

		System.out.println(netIn.readUTF());

		while (true) {
			try {
				BufferedReader userIn = new BufferedReader(new InputStreamReader(System.in));
				System.out.println("command: ");

				StringTokenizer command = new StringTokenizer(userIn.readLine(), " ");

				if (command.countTokens() < 2 && "QUIT".equalsIgnoreCase(command.nextToken())) {
					netOut.writeUTF("QUIT");
					netOut.flush();
					break;
				}

				String instruction = (command.hasMoreTokens()) ? command.nextToken().toUpperCase() : "";
				String source = (command.hasMoreTokens()) ? command.nextToken() : "";
				String dest = (command.hasMoreTokens()) ? command.nextToken() : "";
				switch (instruction) {
				case "PUT":
					put(source, dest);
					break;
				case "GET":
					get(source, dest);
					break;
				default:
					System.out.println("Command not found");
				}
			} catch (CommunicationException e) {
				System.out.println(e.getMessage());
			}
		}
		socket.close();
	}

	/**
	 * Get a file from server
	 *
	 * @param source
	 *            path name of source file
	 * @param dest
	 *            path mane of destination file
	 * @throws IOException
	 *             exception throw by IO
	 */
	private void get(String source, String dest) throws IOException {
		netOut.writeUTF("GET " + source);
		netOut.flush();

		int error;
		if ((error = netIn.readInt()) == ErrorCode.NO_ERROR) {
			BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(dest));

			long fileSize = netIn.readLong();

			byte[] buffer = new byte[1024 * 1024];
			int bytesNeedReading = (int) (fileSize > buffer.length ? buffer.length : fileSize);
			int size;
			while (fileSize > 0 && (size = netIn.read(buffer, 0, bytesNeedReading)) > -1) {
				fileOut.write(buffer, 0, size);
				fileSize -= size;
				bytesNeedReading = (int) (fileSize > buffer.length ? buffer.length : fileSize);
			}
			fileOut.close();

			if (netIn.readInt() == ErrorCode.SUCCESSFULL) {
				System.out.println("Succesfull getting");
			}
		} else if (error == ErrorCode.FILE_NOT_FOUND_ERROR) {
			System.out.println("File is not exist");
		} else if (error == ErrorCode.SERVER_ERROR) {
			System.out.println("Server Error");
		}
	}

	private void put(String source, String dest) throws CommunicationException {
		try {
			netOut.writeUTF("PUT " + dest);
			netOut.flush();
			int error;
			if ((error = netIn.readInt()) == ErrorCode.NO_ERROR) {
				System.err.println(error);
				File sourceFile = new File(source);
				BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(sourceFile));

				netOut.writeLong(sourceFile.length());
				netOut.flush();

				byte[] buffer = new byte[1024 * 1024];
				int size;
				while ((size = fileIn.read(buffer)) > -1) {
					netOut.write(buffer, 0, size);
				}

				netOut.flush();
				fileIn.close();

				if (netIn.readInt() == ErrorCode.SUCCESSFULL) {
					System.out.println("Succesfull putting");
				}
			} else if (error == ErrorCode.SERVER_ERROR) {
				System.out.println("Server Error");
			}
		} catch (FileNotFoundException e) {
			throw new CommunicationException("File is not exist");
		} catch (IOException e) {
			throw new CommunicationException(e.getMessage());
		}
	}
}
