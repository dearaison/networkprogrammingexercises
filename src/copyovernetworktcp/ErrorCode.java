package copyovernetworktcp;

public class ErrorCode {
	public static final int SUCCESSFULL = 200;
	public static final int NO_ERROR = 0;
	public static final int COMMAND_NOT_FOUND_ERROR = -201;
	public static final int SERVER_ERROR = 500;
	public static final int FILE_NOT_FOUND_ERROR = 404;

}
