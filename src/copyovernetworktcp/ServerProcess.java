package copyovernetworktcp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.StringTokenizer;

import javax.naming.CommunicationException;

public class ServerProcess extends Thread {
	private Socket socket;
	private DataInputStream netIn;
	private DataOutputStream netOut;

	public ServerProcess(Socket socket) {
		this.socket = socket;
	}

	private void get(String pathname) throws CommunicationException {
		try {
			File file = new File(pathname);
			BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(file));

			netOut.writeInt(ErrorCode.NO_ERROR);
			netOut.flush();

			netOut.writeLong(file.length());
			netOut.flush();

			byte[] buffer = new byte[1014 * 1024];
			int size;
			while ((size = fileIn.read(buffer)) > -1) {
				netOut.write(buffer, 0, size);
			}
			netOut.flush();
			fileIn.close();

			netOut.writeInt(ErrorCode.SUCCESSFULL);
			netOut.flush();
		} catch (FileNotFoundException e) {
			throw new CommunicationException(ErrorCode.FILE_NOT_FOUND_ERROR + "");
		} catch (IOException e) {
			throw new CommunicationException(ErrorCode.SERVER_ERROR + "");
		}
	}

	private void put(String pathname) throws CommunicationException {
		try {
			BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(pathname));

			netOut.writeInt(ErrorCode.NO_ERROR);
			netOut.flush();

			long fileSize = netIn.readLong();

			byte[] buffer = new byte[1024 * 1024];
			int bytesNeedReading = (int) (fileSize > buffer.length ? buffer.length : fileSize);
			int size;
			while (fileSize > 0 && (size = netIn.read(buffer, 0, bytesNeedReading)) > -1) {
				fileOut.write(buffer, 0, size);
				fileSize -= size;
				bytesNeedReading = (int) (fileSize > buffer.length ? buffer.length : fileSize);
			}
			fileOut.close();

			netOut.writeInt(ErrorCode.SUCCESSFULL);
			netOut.flush();
		} catch (IOException e) {
			throw new CommunicationException(ErrorCode.SERVER_ERROR + "");
		}
	}

	@Override
	public void run() {
		try {
			netIn = new DataInputStream(new BufferedInputStream(this.socket.getInputStream()));
			netOut = new DataOutputStream(new BufferedOutputStream(this.socket.getOutputStream()));
			netOut.writeUTF("Welcome to File cloud!");
			netOut.flush();
			while (true) {
				try {

					StringTokenizer command = new StringTokenizer(netIn.readUTF(), " ");

					if (command.countTokens() < 2 && "QUIT".equalsIgnoreCase(command.nextToken())) {
						netOut.writeUTF("QUIT");
						break;
					}

					String instruction = (command.hasMoreTokens()) ? command.nextToken().toUpperCase() : "";
					String parameter = (command.hasMoreTokens()) ? command.nextToken() : "";

					switch (instruction) {
					case "PUT":
						put(parameter);
						break;
					case "GET":
						get(parameter);
						break;
					default:
						netOut.writeInt(ErrorCode.COMMAND_NOT_FOUND_ERROR);
					}
				} catch (CommunicationException e) {
					netOut.writeInt(Integer.parseInt(e.getMessage()));
				}
			}
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
