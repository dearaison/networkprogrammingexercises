package copyovernetworktcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static void main(String[] args) {
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(2000);
			while (true) {
				Socket socket = serverSocket.accept();
				ServerProcess process = new ServerProcess(socket);
				process.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	} 
}
