package rmi.copyfile.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import rmi.copyfile.server.Downloader;
import rmi.copyfile.server.Uploader;

public class Client {
	public static final void download(String sf, String df) throws NotBoundException, IOException {
		Registry registry = LocateRegistry.getRegistry("localhost", 12345);
		Downloader downloader = (Downloader) registry.lookup("DOWNLOADSERVICE");

		BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(df));
		downloader.openfile(sf);

		byte[] buffer;
		while ((buffer = downloader.readData()) != null) {
			fileOut.write(buffer);
		}

		fileOut.close();
		downloader.closeFile();
	}

	public static void main(String[] args) throws RemoteException, NotBoundException {
		BufferedReader commandInput = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			try {
				System.out.println("Welcome...");
				StringTokenizer line = new StringTokenizer(commandInput.readLine(), "\t");
				if (line.countTokens() == 1 && "QUIT".equalsIgnoreCase(line.nextToken())) {
					break;
				}
				if (line.countTokens() != 3) {
					System.out.println("Command syntax is wrong!!!");
				}

				String instruction = line.nextToken().toUpperCase();
				String sf = line.nextToken();
				String df = line.nextToken();

				switch (instruction) {
				case "UPLOAD":
					upload(sf, df);
					break;

				case "DOWNLOAD":
					download(sf, df);
					break;

				default:
					System.out.println("Command not found!!!");
					break;
				}

			} catch (IOException e) {
				System.out.println(e.getMessage());
			} catch (NoSuchElementException e) {
				System.out.println("Command syntax is wrongg!!!");
			}
		}
	}

	public static final void upload(String sf, String df) throws NotBoundException, IOException {
		Registry registry = LocateRegistry.getRegistry("localhost", 12345);
		Uploader uploader = (Uploader) registry.lookup("UPLOADSERVICE");

		BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(sf));
		uploader.createfile(df);

		byte[] buffer = new byte[102400];
		int size;
		while ((size = fileIn.read(buffer)) > -1) {
			uploader.writeData(buffer, 0, size);
		}

		fileIn.close();
		uploader.closeFile();
	}

}
