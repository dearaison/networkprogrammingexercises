package rmi.copyfile.server;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class DownloadService extends UnicastRemoteObject implements Downloader {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6418290931537025632L;
	private BufferedInputStream sourceIn;

	public DownloadService() throws RemoteException {
		super();
	}

	@Override
	public void closeFile() throws RemoteException {
		try {
			sourceIn.close();
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	@Override
	public void openfile(String sf) throws RemoteException {
		try {
			sourceIn = new BufferedInputStream(new FileInputStream(sf));
		} catch (FileNotFoundException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	@Override
	public byte[] readData() throws RemoteException {
		byte[] buffer = new byte[102400];
		int size;
		try {
			size = sourceIn.read(buffer);

			if (size == -1) {
				return null;
			}

			byte[] data = new byte[size];
			System.arraycopy(buffer, 0, data, 0, size);
			return data;
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

}
