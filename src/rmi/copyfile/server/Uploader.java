package rmi.copyfile.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Uploader extends Remote {
	void closeFile() throws RemoteException;

	void createfile(String df) throws RemoteException;

	void writeData(byte[] data, int offset, int length) throws RemoteException;
}
