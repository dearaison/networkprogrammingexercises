package rmi.copyfile.server;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class UploadService extends UnicastRemoteObject implements Uploader {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4041851310804386464L;
	private BufferedOutputStream desOut;

	public UploadService() throws RemoteException {
		super();
	}

	@Override
	public void closeFile() throws RemoteException {
		try {
			desOut.close();
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

	@Override
	public void createfile(String df) throws RemoteException {
		try {
			desOut = new BufferedOutputStream(new FileOutputStream(df));
		} catch (FileNotFoundException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

	@Override
	public void writeData(byte[] data, int offset, int length) throws RemoteException {
		try {
			desOut.write(data, offset, length);
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

}
