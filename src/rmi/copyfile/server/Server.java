package rmi.copyfile.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
	public static void main(String[] args) throws RemoteException {
		Registry registry = LocateRegistry.createRegistry(12345);

		Downloader downloader = new DownloadService();
		Uploader uploader = new UploadService();

		registry.rebind("DOWNLOADSERVICE", downloader);
		registry.rebind("UPLOADSERVICE", uploader);

		System.out.println("Server running");
	}
}
