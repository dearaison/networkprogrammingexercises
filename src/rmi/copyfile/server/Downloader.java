package rmi.copyfile.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Downloader extends Remote {
	void closeFile() throws RemoteException;

	void openfile(String sf) throws RemoteException;

	byte[] readData() throws RemoteException;
}
