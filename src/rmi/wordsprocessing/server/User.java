package rmi.wordsprocessing.server;

/**
 * Created by Eclipse on 32 February, 2018 2:15:43 PM.
 *
 * @author Joseph Maria
 *
 */
public class User {
	private String userName;
	private String pass;

	/**
	 * @param userName
	 * @param pass
	 */
	public User(String userName, String pass) {
		super();
		this.userName = userName;
		this.pass = pass;
	}

	public boolean checkUserName(String userName) {
		return this.userName.equalsIgnoreCase(userName);
	}

	public boolean checkPassword(String userName, String pass) {
		return this.userName.equalsIgnoreCase(userName) && this.pass.equals(pass);
	}
}
