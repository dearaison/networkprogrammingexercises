package rmi.pop3uploaddownload.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Eclipse on 31 January, 2018 3:18:37 PM.
 *
 * @author Joseph Maria
 *
 */
public class Logging extends UnicastRemoteObject implements Loginable {
	private static HashMap<String, String> userData = new HashMap<String, String>();

	/**
	 * 
	 */
	private static final long serialVersionUID = -189162142336838612L;

	/**
	 * @throws RemoteException
	 */
	protected Logging() throws RemoteException {
		super();
		userData.put("TOMOE", "Truongem741");
		userData.put("YUNA", "Truongem852");
		userData.put("TOMOYO", "Truongem963");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rmi.pop3uploaddownload.server.Loginable#checkPassword(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String checkPassword(String username, String pass) throws RemoteException {
		if (userData.containsKey(username.toUpperCase()) && userData.get(username.toUpperCase()).equals(pass)) {
			return UUID.randomUUID().toString();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rmi.pop3uploaddownload.server.Loginable#checkUser(java.lang.String)
	 */
	@Override
	public boolean checkUser(String username) throws RemoteException {
		return userData.containsKey(username.toUpperCase());
	}

}
