package rmi.pop3uploaddownload.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Eclipse on 31 January, 2018 3:23:00 PM.
 *
 * @author Joseph Maria
 *
 */
public class Server {
	public static void main(String[] args) throws RemoteException {
		Registry registry = LocateRegistry.createRegistry(12345);

		Loginable loginable = new Logging();
		Uploader uploader = new Uploading();
		Downloader downloader = new Downloading();

		registry.rebind("login", loginable);
		registry.rebind("uploader", uploader);
		registry.rebind("downloader", downloader);

		System.out.println("Server Running");
	}
}
