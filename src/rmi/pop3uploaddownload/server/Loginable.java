package rmi.pop3uploaddownload.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Eclipse on 31 January, 2018 3:14:39 PM.
 *
 * @author Joseph Maria
 *
 */
public interface Loginable extends Remote {
	public String checkPassword(String username, String pass) throws RemoteException;

	public boolean checkUser(String username) throws RemoteException;
}
