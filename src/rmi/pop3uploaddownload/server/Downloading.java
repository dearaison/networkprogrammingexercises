package rmi.pop3uploaddownload.server;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Eclipse on 31 January, 2018 4:51:29 PM.
 *
 * @author Joseph Maria
 *
 */
public class Downloading extends UnicastRemoteObject implements Downloader {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2263369926576330438L;
	private final HashMap<String, InputStream> FILE;

	/**
	 * @throws RemoteException
	 */
	protected Downloading() throws RemoteException {
		super();
		FILE = new HashMap<String, InputStream>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rmi.pop3uploaddownload.server.Downloader#closeFile(java.lang.String)
	 */
	@Override
	public void closeFile(String identifyID) throws RemoteException {
		try {
			FILE.get(identifyID).close();
			FILE.remove(identifyID);
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rmi.pop3uploaddownload.server.Downloader#openFile(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void openFile(String identifyID, String sf) throws RemoteException {
		try {
			FILE.put(identifyID, new BufferedInputStream(new FileInputStream(sf)));
		} catch (FileNotFoundException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rmi.pop3uploaddownload.server.Downloader#read(java.lang.String)
	 */
	@Override
	public byte[] read(String identifyID) throws RemoteException {
		try {
			byte[] bs = new byte[1024 * 1024];
			int size;
			if ((size = FILE.get(identifyID).read(bs)) <= -1) {
				return null;
			} else {
				return Arrays.copyOf(bs, size);
			}
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

}
