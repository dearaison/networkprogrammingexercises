package rmi.pop3uploaddownload.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Eclipse on 31 January, 2018 3:15:37 PM.
 *
 * @author Joseph Maria
 *
 */
public interface Uploader extends Remote {
	public void closeFile(String identifyID) throws RemoteException;

	public void createFile(String identifyID, String df) throws RemoteException;



	public void write(String identifyID, byte[] buffer, int offset, int length) throws RemoteException;
}
