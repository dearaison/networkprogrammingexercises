package rmi.pop3uploaddownload.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Eclipse on 31 January, 2018 4:50:11 PM.
 *
 * @author Joseph Maria
 *
 */
public interface Downloader extends Remote {
	public void closeFile(String identifyID) throws RemoteException;

	public void openFile(String identifyID, String sf) throws RemoteException;

	public byte[] read(String identifyID) throws RemoteException;

}
