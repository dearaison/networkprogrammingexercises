package rmi.pop3uploaddownload.server;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

/**
 * Created by Eclipse on 31 January, 2018 4:35:28 PM.
 *
 * @author Joseph Maria
 *
 */
public class Uploading extends UnicastRemoteObject implements Uploader {
	/**
	 * 
	 */
	private static final long serialVersionUID = -925797790310920950L;
	private final HashMap<String, OutputStream> FILE;

	/**
	 * @throws RemoteException
	 */
	protected Uploading() throws RemoteException {
		super();
		FILE = new HashMap<String, OutputStream>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see rmi.pop3uploaddownload.server.UploaderAndDownloader#closeFile(java.lang.
	 * String)
	 */
	@Override
	public void closeFile(String identifyID) throws RemoteException {
		try {
			FILE.get(identifyID).close();
			FILE.remove(identifyID);
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * rmi.pop3uploaddownload.server.UploaderAndDownloader#createFile(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public void createFile(String identifyID, String df) throws RemoteException {
		try {
			FILE.put(identifyID, new BufferedOutputStream(new FileOutputStream(df)));
		} catch (FileNotFoundException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * rmi.pop3uploaddownload.server.UploaderAndDownloader#write(java.lang.String,
	 * byte[], int, int)
	 */
	@Override
	public void write(String identifyID, byte[] buffer, int offset, int length) throws RemoteException {
		try {
			FILE.get(identifyID).write(buffer, offset, length);
		} catch (IOException e) {
			throw new RemoteException(e.getMessage(), e);
		}

	}

}
