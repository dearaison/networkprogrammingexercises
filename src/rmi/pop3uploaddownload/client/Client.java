package rmi.pop3uploaddownload.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.StringTokenizer;

import rmi.pop3uploaddownload.server.Downloader;
import rmi.pop3uploaddownload.server.Loginable;
import rmi.pop3uploaddownload.server.Uploader;

/**
 * Created by Eclipse on 31 January, 2018 3:27:03 PM.
 *
 * @author Joseph Maria
 *
 */
public class Client {
	private static final String HOST = "127.0.0.1";
	private static final int PORT = 12345;

	private final Loginable LOGINABLE;
	private final Uploader UPLOADER;
	private final Downloader DOWNLOADER;
	private final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

	private String username = null;
	private String identifyID = null;

	private String instruction;
	private String param;
	private String sf;
	private String df;

	/**
	 * @throws RemoteException
	 * @throws NotBoundException
	 * 
	 */
	public Client() throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry(HOST, PORT);
		LOGINABLE = (Loginable) registry.lookup("login");
		UPLOADER = (Uploader) registry.lookup("uploader");
		DOWNLOADER = (Downloader) registry.lookup("downloader");
	}

	/**
	 * @throws RemoteException
	 * 
	 */
	private void checkUser() throws RemoteException {
		if (LOGINABLE.checkUser(param)) {
			username = param;
			System.out.println("User exist.");
		} else {
			System.out.println("User not exist.");
		}

	}

	/**
	 * @throws IOException
	 * 
	 */
	private void download() throws IOException {
		DOWNLOADER.openFile(identifyID, sf);
		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(df));
		byte[] bs = new byte[1024 * 1024];
		while ((bs = DOWNLOADER.read(identifyID)) != null) {
			outputStream.write(bs);
		}
		outputStream.close();
		DOWNLOADER.closeFile(identifyID);
		System.out.println("Download successfully");
	}

	/**
	 * @throws RemoteException
	 * 
	 */
	private void Login() throws RemoteException {
		if (username == null) {
			System.out.println("Invalid logging");
			return;
		}

		String temp = LOGINABLE.checkPassword(username, param);
		if (temp != null) {
			identifyID = temp;
			System.out.println("Password Right.");
		} else {
			System.out.println("Password wrong.");
		}
	}

	/**
	 * @param command
	 */
	private void processCommand(String command) {
		StringTokenizer tokenizer = new StringTokenizer(command, " ");
		if (tokenizer.countTokens() == 2) {
			instruction = tokenizer.nextToken().toUpperCase();
			param = tokenizer.nextToken();
		} else if (tokenizer.countTokens() == 3) {
			instruction = tokenizer.nextToken().toUpperCase();
			sf = tokenizer.nextToken();
			df = tokenizer.nextToken();
		} else {
			instruction = "";
		}

	}

	/**
	 * @throws IOException
	 * 
	 */
	public void runClient() throws IOException {
		System.out.println("Welcome to File Management Service");

		while (identifyID == null) {
			String command = READER.readLine();

			if ("QUICK".equalsIgnoreCase(command)) {
				break;
			}

			processCommand(command);

			switch (instruction) {
			case "USER":
				checkUser();
				break;

			case "PASS":
				Login();
				break;

			default:
				System.out.println("Command not found");
				break;
			}
		}

		while (identifyID != null) {
			String command = READER.readLine();

			if ("QUICK".equalsIgnoreCase(command)) {
				break;
			}

			processCommand(command);
			switch (instruction) {
			case "UPLOAD":
				upload();
				break;

			case "DOWNLOAD":
				download();
				break;

			default:
				System.out.println("Command not found");
				break;
			}

		}

	}

	/**
	 * @throws IOException
	 * 
	 */
	private void upload() throws IOException {
		UPLOADER.createFile(identifyID, df);
		BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(sf));
		byte[] bs = new byte[1024 * 1024];
		int size;
		while ((size = inputStream.read(bs)) > -1) {
			UPLOADER.write(identifyID, bs, 0, size);
		}
		inputStream.close();
		UPLOADER.closeFile(identifyID);
		System.out.println("Upload successfully");
	}

	public static void main(String[] args) throws RemoteException, IOException, NotBoundException {
		new Client().runClient();
	}
}
