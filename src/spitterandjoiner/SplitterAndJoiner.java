package spitterandjoiner;

import java.io.*;

public class SplitterAndJoiner {

    private static String generateEXT(int count) {
        String result = "" + count;
        while (result.length() < 3) {
            result = "0" + result;
        }
        return result;
    }

    public static void joinFile(String pathName) throws IOException {
        File source = new File(pathName);
        if (source.exists()) {
            String outFilePathName = pathName.substring(0, pathName.lastIndexOf("."));
            OutputStream BOS = new BufferedOutputStream(new FileOutputStream(outFilePathName));
            int count = 1;
            File inFile;
            String inFilePathName = outFilePathName + "." + generateEXT(count);
            while ((inFile = new File(inFilePathName)).exists()) {
                InputStream BIS = new BufferedInputStream(new FileInputStream(inFile));
                transfer(BIS, BOS, inFile.length());
                BIS.close();
                count++;
                inFile.delete();
                System.out.println("Join from: " + inFilePathName);
                inFilePathName = outFilePathName + "." + generateEXT(count);
            }
            BOS.close();
        }
    }

    public static void main(String[] args) throws IOException {
        String pathName = "B:\\baotruong11a\\TestFolder\\Kanji Look and Learn Workbook Kaitou.pdf";
        // splitFile(pathName, 1024 * 1024 * 1);
        //splitFile(pathName, 3);
        joinFile(pathName + ".001");
    }

    public static void splitFile(String pathname, int numberOfFile) throws IOException {
        File sourceFile = new File(pathname);
        if (sourceFile.exists()) {
            InputStream BIS = new BufferedInputStream(new FileInputStream(pathname));
            int partSize = (int) (sourceFile.length() / numberOfFile);
            int remainData = (int) (sourceFile.length() % numberOfFile);

            splitToFile(partSize, sourceFile, BIS, numberOfFile);

            if (remainData > 0) {
                String newFilePathName = sourceFile.getAbsolutePath() + "." + generateEXT((numberOfFile));
                OutputStream BOS = new BufferedOutputStream(new FileOutputStream(newFilePathName, true));
                transfer(BIS, BOS, partSize);
                BOS.close();
                System.out.println("Split into: " + newFilePathName);
            }
            BIS.close();
        }
    }

    public static void splitFile(String pathname, long partSize) throws IOException {
        File sourceFile = new File(pathname);
        if (sourceFile.exists()) {
            InputStream BIS = new BufferedInputStream(new FileInputStream(pathname));
            int numberOfFile = (int) (sourceFile.length() / partSize);
            int remainData = (int) (sourceFile.length() % partSize);

            splitToFile(partSize, sourceFile, BIS, numberOfFile);

            if (remainData > 0) {
                String newFilePathName = sourceFile.getAbsolutePath() + "." + generateEXT((numberOfFile + 1));
                OutputStream BOS = new BufferedOutputStream(new FileOutputStream(newFilePathName));
                transfer(BIS, BOS, partSize);
                BOS.close();
                System.out.println("Split into: " + newFilePathName);
            }
            BIS.close();
        }
    }

    private static void splitToFile(long partSize, File sourceFile, InputStream BIS, int numberOfFile) throws IOException {
        for (int i = 1; i <= numberOfFile; i++) {
            String newFilePathName = sourceFile.getAbsolutePath() + "." + generateEXT(i);
            OutputStream BOS = new BufferedOutputStream(new FileOutputStream(newFilePathName));
            transfer(BIS, BOS, partSize);
            BOS.close();
            System.out.println("Split into: " + newFilePathName);
        }
    }

    private static void transfer(InputStream source, OutputStream destination, long partSize) throws IOException {
        byte[] buffer = new byte[1024 * 1024];
        int size;
        long remainByte = partSize;
        int bytesNeedReading = (int) ((remainByte > buffer.length) ? buffer.length : remainByte);
        while (remainByte > 0 && (size = source.read(buffer, 0, bytesNeedReading)) > -1) {
            destination.write(buffer, 0, size);
            remainByte -= size;
            bytesNeedReading = (int) ((remainByte > buffer.length) ? buffer.length : remainByte);
        }
    }
}
