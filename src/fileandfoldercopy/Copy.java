package fileandfoldercopy;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Copy {

	/**
	 * Copy a file or directory
	 * 
	 * @param sourcePathname
	 *            the source pathname which denote the file or directory need to be
	 *            copied
	 * @param targetPathname
	 *            the target pathname which denote the new file or directory
	 *            AbsolutePath
	 * @return void
	 */
	public static void copy(String sourcePathname, String targetPathname) throws IOException {
		File source = new File(sourcePathname);
		File target = new File(targetPathname);
		if (source.exists()) {
			if (source.isDirectory()) {
				copyDirectory(source, target);
			} else
				copyFile(source, target);
		}
	}

	/**
	 * Copy a directory
	 * 
	 * @param sourceDirectory
	 *            the source pathname which denote the directory need to be copied
	 * @param targetDirectory
	 *            the target pathname which denote the new directory AbsolutePath
	 * @return void
	 */
	private static void copyDirectory(File sourceDirectory, File targetDirectory) throws IOException {
		long beginningTime = System.currentTimeMillis();
		// Make the target directory if it's not exists.
		if (!targetDirectory.exists()) {
			targetDirectory.mkdirs();
		}

		// Copy all files and directories in source directory into target directory.
		for (File content : sourceDirectory.listFiles()) {
			copy(content.getAbsolutePath(), targetDirectory.getAbsolutePath() + File.separator + content.getName());
		}
		long endingTime = System.currentTimeMillis();
		System.out.println("Copy: " + sourceDirectory.getName() + " in: " + (endingTime - beginningTime) + " ms.");
	}

	/**
	 * Copy a file
	 * 
	 * @param sourceFile
	 *            the source pathname which denote the file need to be copied
	 * @param targetFile
	 *            the target pathname which denote the new file AbsolutePath
	 * @return void
	 */
	private static void copyFile(File sourceFile, File targetFile) throws IOException {
		InputStream BIS = new BufferedInputStream(new FileInputStream(sourceFile));
		OutputStream BOS = new BufferedOutputStream(new FileOutputStream(targetFile));
		byte[] buffer = new byte[1024 * 1024];
		int size;
		long beginningTime = System.currentTimeMillis();
		while ((size = BIS.read(buffer)) > -1) {
			BOS.write(buffer, 0, size);
		}
		long endingTime = System.currentTimeMillis();
		BIS.close();
		BOS.close();
		System.out.println("Copy: " + sourceFile.getName() + " in: " + (endingTime - beginningTime) + " ms.");

	}

	public static void main(String[] args) throws IOException {
		String sourcePathname = "B:\\baotruong11a\\TestFolder\\test1";
		String targetPathname = sourcePathname + " - copy";
		copy(sourcePathname, targetPathname);
	}

}
