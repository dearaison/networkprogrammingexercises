package fileandfoldercopy;

import java.io.File;

public class CopyUtils {

	public static String generateNewPathName(String pathname, String targetLocation) {
		// TODO to much thing to do to make your copy like system copy.
		File sourceFile = new File(pathname);
		// File target = new File(targetLocation);
		if (sourceFile.exists()) {
			String sourceFileParent = sourceFile.getParent();
			String sourceFileName = sourceFile.getName();
			int count = 0;
			String tempNewFilePathName = (sourceFileParent != null)
					? sourceFileParent + sourceFileName.substring(0, sourceFileName.lastIndexOf('.') + 1) + "-copy"
							+ count + sourceFileName.substring(sourceFileName.lastIndexOf("."))
					: sourceFileName.substring(0, sourceFileName.lastIndexOf('.') + 1) + "-copy" + count
							+ sourceFileName.substring(sourceFileName.lastIndexOf("."));
			File newFile = new File(tempNewFilePathName);
			while (newFile.exists()) {
				count++;
				tempNewFilePathName = (sourceFileParent != null)
						? sourceFileParent + sourceFileName.substring(0, sourceFileName.lastIndexOf('.') + 1) + "-copy"
								+ count + sourceFileName.substring(sourceFileName.lastIndexOf("."))
						: sourceFileName.substring(0, sourceFileName.lastIndexOf('.') + 1) + "-copy" + count
								+ sourceFileName.substring(sourceFileName.lastIndexOf("."));
				newFile = new File(tempNewFilePathName);
			}
			return tempNewFilePathName;
		} else {
			return null;
		}
	}

}
