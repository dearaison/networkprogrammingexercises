package io.file;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

/**
 * Created by Eclipse on 30 January, 2018 9:02:30 AM.
 *
 * @author Joseph Maria
 *
 */
public class TestCopying {
	@Test
	public void test() {
		Copying test1 = new Copying();
		String sFile = "A:\\baotr\\Videos\\Fatestay night\\Fatestay night\\Fatestay night - Vietsub Tap 1 - Ani4u.mp4",
				destFile = "B:\\baotr\\Test - Copy\\Fatestay night - Vietsub Tap 1 - Ani4u.mp4";
		File dest;

		Long startTime, endTime;

		long aver1 = 0, aver2 = 0, aver3 = 0, aver4 = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 100; j++) {
				startTime = System.nanoTime();
				test1.copyFile(sFile, destFile);
				endTime = System.nanoTime();
				aver1 += endTime - startTime;
				dest = new File(destFile);
				dest.delete();

				startTime = System.nanoTime();
				test1.copyFile2(sFile, destFile);
				endTime = System.nanoTime();
				aver2 = aver2 += endTime - startTime;
				dest = new File(destFile);
				dest.delete();

				startTime = System.nanoTime();
				test1.copyFile3(sFile, destFile);
				endTime = System.nanoTime();
				aver3 += endTime - startTime;
				dest = new File(destFile);
				dest.delete();

				startTime = System.nanoTime();
				test1.copyFile4(sFile, destFile);
				endTime = System.nanoTime();
				aver4 += endTime - startTime;
				dest = new File(destFile);
				dest.delete();

			}

			System.out.println(aver1 /= 100);
			System.out.println(aver2 /= 100);
			System.out.println(aver3 /= 100);
			System.out.println(aver4 /= 100);
			long min = Math.min(aver1, Math.min(aver2, Math.min(aver3, aver4)));
			assertEquals(aver3, min);
			System.out.println("Min: " + min);
		}
	}

}
