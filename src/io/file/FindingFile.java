package io.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Eclipse on 29 January, 2018 9:43:19 AM.
 *
 * @author Joseph Maria
 *
 */
public class FindingFile {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String path = "B:\\baotr\\Test - Copy";
		System.out.println(new FindingFile().findFile("New Microsoft PowerPoint 97-2003 Presentation.ppt", path));
	}

	public boolean findFile(String pattern, String path) throws IOException {
		File in = new File(path);

		if (!in.exists()) {
			throw new FileNotFoundException();
		}
		if (in.isDirectory()) {
			for (File file : in.listFiles()) {
				if (findFile(pattern, file.getCanonicalPath())) {
					return true;
				}
			}
		}
		return in.getName().contains(pattern);
	}

}
