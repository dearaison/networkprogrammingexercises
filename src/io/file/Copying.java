package io.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * Created by Eclipse on 30 January, 2018 7:48:28 AM.
 *
 * @author Joseph Maria
 *
 */
public class Copying {
	public static void main(String[] args) throws IOException {
		Copying copying = new Copying();
		String sFile = "B:\\baotr\\Test - Copy";
		String destFile = "B:\\baotr\\Test - Copy2";
		copying.copy(sFile, destFile);
	}

	public boolean copy(String sFile, String destFile) throws IOException {
		if (new File(sFile).isFile()) {
			return copyFile3(sFile, destFile);
		} else {
			return copyDirectory(sFile, destFile);
		}
	}

	/**
	 * @param sFile
	 * @param destFile
	 * @return
	 * @throws IOException
	 */
	private boolean copyDirectory(String sFile, String destFile) throws IOException {
		File destDirectory = new File(destFile);
		boolean res = true;
		if (!destDirectory.exists()) {
			destDirectory.mkdirs();
		}
		for (File file : new File(sFile).listFiles()) {
			res &= copy(file.getCanonicalPath(), destFile + File.separator + file.getName());
		}
		return res;
	}

	public boolean copyFile(String sFile, String destFile) {
		try {
			File source = new File(sFile);
			BufferedInputStream BIS = new BufferedInputStream(new FileInputStream(source));
			BufferedOutputStream BOS = new BufferedOutputStream(new FileOutputStream(destFile));

			byte[] buffer = new byte[1024 * 1024];
			int size;
			while ((size = BIS.read(buffer)) > -1) {
				BOS.write(buffer, 0, size);
			}

			BIS.close();
			BOS.close();
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean copyFile2(String sFile, String destFile) {
		try {
			File source = new File(sFile);
			BufferedInputStream BIS = new BufferedInputStream(new FileInputStream(source));
			BufferedOutputStream BOS = new BufferedOutputStream(new FileOutputStream(destFile));

			BIS.transferTo(BOS);

			BIS.close();
			BOS.close();
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean copyFile3(String sFile, String destFile) {
		try {
			Files.copy(new File(sFile).toPath(), new File(destFile).toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean copyFile4(String sFile, String destFile) {
		try {
			File source = new File(sFile);

			FileInputStream FIS = new FileInputStream(source);
			FileOutputStream FOS = new FileOutputStream(destFile);

			FileChannel iChannel = FIS.getChannel();
			FileChannel oChannel = FOS.getChannel();

			iChannel.transferTo(0, source.length(), oChannel);

			FIS.close();
			FOS.close();
			iChannel.close();
			oChannel.close();
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

}
