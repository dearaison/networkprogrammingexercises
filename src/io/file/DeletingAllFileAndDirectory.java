package io.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Eclipse on 29 January, 2018 8:46:18 AM.
 *
 * @author Joseph Maria
 *
 */
public class DeletingAllFileAndDirectory {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String path = "B:\\baotr\\Test - Copy";
		System.out.println(new DeletingAllFileAndDirectory().DeletingAll(path));
	}

	public boolean DeletingAll(String path) throws IOException {
		File in = new File(path);
		if (!in.exists()) {
			throw new FileNotFoundException(path);
		}

		if (in.isDirectory()) {
			for (File file : in.listFiles()) {
				DeletingAll(file.getCanonicalPath());
			}
		}
		return in.delete();
	}

}
