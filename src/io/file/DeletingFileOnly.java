package io.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Eclipse on 29 January, 2018 9:15:18 AM.
 *
 * @author Joseph Maria
 *
 */
public class DeletingFileOnly {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String path = "B:\\baotr\\Test - Copy";
		System.out.println(new DeletingFileOnly().deleteFiles(path));
	}

	public boolean deleteFiles(String path) throws IOException {
		boolean result = true;
		File in = new File(path);
		if (!in.exists()) {
			throw new FileNotFoundException(path);
		}
		if (in.isFile()) {
			result &= in.delete();
		}
		if (in.isDirectory()) {
			for (File file : in.listFiles()) {
				result &= deleteFiles(file.getCanonicalPath());
			}
		}
		return result;
	}

}
