package studenttcptext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Server implements Runnable {
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(12345);
		System.out.println("Server is running!");
		while (true) {
			Server server = new Server(serverSocket.accept());
			new Thread(server).start();
			System.out.println("Client is connected!");
		}
	}
	private Socket socket;
	private BufferedReader in;

	private PrintWriter out;

	HashMap<String, Student> students = new HashMap<String, Student>();

	public Server(Socket socket) throws IOException {
		this.socket = socket;
		this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		this.out = new PrintWriter(this.socket.getOutputStream(), true);
	}

	@Override
	public void run() {
		List<String> data = new LinkedList<String>();
		data.add("Client co the thao tao voi cac lech sau day: ");
		data.add("THEM_MSV_HOVATEN_SDT_DIEMMONHOC");
		data.add("SUA_MSV_HOVATEN_SDT_DIEMMONHOC");
		data.add("XOA_MSV");
		data.add("XEM_MSV");
		data.add("XEMTATCA");
		try {
			out.println("Welcome to student management service.");
			while (true) {
				writeML(data);
				String line = in.readLine();
				StringTokenizer token = new StringTokenizer(line, "_");
				if (token.countTokens() == 0) {
					out.println("Sai cu phap.");
					continue;
				}
				String comand = token.nextToken();
				switch (comand) {
				case "THEM":
					them(line);
					break;
				case "XOA":
					xoa();
					break;
				case "SUA":
					sua();
					break;
				case "XEM":
					xem();
					break;
				case "XEMTATCA":
					xemtatca();
					break;

				default:
					out.println("Sai cu phap.");
					continue;
				}
				this.socket.close();
			}
		} catch (Exception e) {

		}
	}

	private void sua() {
		// TODO Auto-generated method stub

	}

	private void them(String line) throws IOException {
		while (true) {
			StringTokenizer token = new StringTokenizer(line, "_");
			token.nextToken();
			them(token.nextToken(), token.nextToken(), token.nextToken(), token.nextToken());
			out.println("Ban co muon them tiep sv nua khong (Y/N)?");
			line = in.readLine();
			out.println("Moi nhap sv");
			if ("Y".equals(line)) {
				line = in.readLine();
				token = new StringTokenizer(line, "_");
				if (token.countTokens() < 0 || !"THEM".equals(token.nextToken())) {
					out.println("Sai cu phap.");
					return;
				} else {
					continue;
				}
			} else {
				return;
			}
		}
	}

	private void them(String msv, String name, String phone, String score) {
		try {
			students.put(msv, new Student(msv, name, phone, Double.parseDouble(score)));
			out.println("Them sinh vien ok.");
		} catch (NumberFormatException e) {
			out.println("Diem khong hop le.");
		}
	}

	public void writeML(List<String> list) {
		for (String string : list) {
			out.println(string);
		}
		out.println(".");
	}

	private void xem() {
		// TODO Auto-generated method stub

	}

	private void xemtatca() {
		List<String> list = new LinkedList<String>();
		for (Student sv : students.values()) {
			list.add(sv.toString());
		}
		writeML(list);
	}

	private void xoa() {
		// TODO Auto-generated method stub

	}

}
