package studenttcptext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.StringTokenizer;

public class Client {
	public static void main(String[] args) throws IOException {
		new Client();
	}
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	String ip = "localhost";
	int port = 12345;

	BufferedReader s = new BufferedReader(new InputStreamReader(System.in));

	public Client() throws IOException {
		this.socket = new Socket(ip, port);
		this.in = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		this.out = new PrintWriter(socket.getOutputStream(), true);

		System.out.println(in.readLine());
		while (true) {
			readML();
			String line = s.readLine();
			out.println(line);
			StringTokenizer token = new StringTokenizer(line, "_");
			if (token.countTokens() == 1
					&& "XEMTATCA".equals(token.nextToken())) {
				readML();
			} else
				System.out.println(in.readLine());
		}
	}

	public void readML() throws IOException {
		String line;
		while (true) {
			line = in.readLine();
			if (".".equals(line))
				break;
			System.out.println(line);
		}
	}
}
