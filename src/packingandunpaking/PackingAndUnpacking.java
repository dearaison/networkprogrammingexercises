package packingandunpaking;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

public class PackingAndUnpacking {
	// File format:
	//
	// file 1 position (8 bytes - Long)
	// file 2 position (8 bytes - Long)
	// file 3 position (8 bytes - Long)
	// -1 (8 bytes - Long)
	//
	// file 1 name (unknown size - UTF)
	// file 1 size (8 bytes - Long)
	// file 1 data (unknown size - data)
	//
	// file 2 name (unknown size - UTF)
	// file 2 size (8 bytes - Long)
	// file 2 data (unknown size - data)
	//
	// file 3 name (unknown size - UTF)
	// file 3 size (8 bytes - Long)
	// file 3 data (unknown size - data)

	public static void extract(String pathname, String destinationFolder) throws IOException {
		File sourceFile = new File(pathname);
		if (!sourceFile.exists())
			return;

		RandomAccessFile randomAccessFile = new RandomAccessFile(sourceFile, "rw");
		long filePosition;
		while ((filePosition = randomAccessFile.readLong()) > -1) {
			long currentPosition = randomAccessFile.getFilePointer();
			randomAccessFile.seek(filePosition);
			String currentFileName = randomAccessFile.readUTF();
			File destinationFile = new File(destinationFolder + File.separator + currentFileName);
			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(destinationFile));
			transfer(randomAccessFile, bufferedOutputStream, randomAccessFile.readLong());
			bufferedOutputStream.close();
			randomAccessFile.seek(currentPosition);
		}
	}

	public static void extract(String pathname, String destinationFolder, String fileName) throws IOException {
		File sourceFile = new File(pathname);
		if (!sourceFile.exists())
			return;

		RandomAccessFile randomAccessFile = new RandomAccessFile(sourceFile, "rw");
		long filePosition;
		while ((filePosition = randomAccessFile.readLong()) > -1) {
			long currentPosition = randomAccessFile.getFilePointer();

			randomAccessFile.seek(filePosition);
			String currentFileName = randomAccessFile.readUTF();
			if (currentFileName.equals(fileName)) {
				File destinationFile = new File(destinationFolder + File.separator + fileName);
				BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
						new FileOutputStream(destinationFile));
				transfer(randomAccessFile, bufferedOutputStream, randomAccessFile.readLong());
				bufferedOutputStream.close();
				break;
			}
			randomAccessFile.seek(currentPosition);
		}
	}

	public static void pack(String pathname) throws IOException {
		File sourceFolder = new File(pathname);
		if (!sourceFolder.exists())
			return;

		RandomAccessFile randomAccessFile = new RandomAccessFile(pathname + ".JMA", "rw");
		File[] content = sourceFolder.listFiles();
		for (int i = 0; i < content.length; i++) {
			randomAccessFile.writeLong(0);
		}
		randomAccessFile.writeLong(-1);

		for (int i = 0; i < content.length; i++) {
			long pointerPosition = randomAccessFile.getFilePointer();
			randomAccessFile.seek(i * 8);
			randomAccessFile.writeLong(pointerPosition);
			randomAccessFile.seek(pointerPosition);

			File inFile = content[i];
			randomAccessFile.writeUTF(inFile.getName());
			randomAccessFile.writeLong(inFile.length());

			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(inFile));
			byte[] buffer = new byte[1024 * 1024];
			int size;
			while ((size = bufferedInputStream.read(buffer)) > -1) {
				randomAccessFile.write(buffer, 0, size);
			}
			bufferedInputStream.close();
		}
		randomAccessFile.close();
	}

	private static void transfer(RandomAccessFile randomAccessFile, OutputStream outputStream, long sizeOfFile)
			throws IOException {
		byte[] buffer = new byte[1024 * 1024];
		long remain = sizeOfFile;
		int bytesNeedReading = (int) ((remain > buffer.length) ? buffer.length : remain);
		int size;
		while (remain > 0 && (size = randomAccessFile.read(buffer, 0, bytesNeedReading)) > -1) {
			outputStream.write(buffer, 0, size);
			remain -= size;
			bytesNeedReading = (int) ((remain > buffer.length) ? buffer.length : remain);
		}
	}

	// public static void main(String[] args) throws IOException {
	// String sourceFolder = "B:\\baotruong11a\\TestFolder\\Test_Packing";
	// String packedFile = "B:\\baotruong11a\\TestFolder\\Test_Packing.JMA";
	// String extractingFolder = "B:\\baotruong11a\\TestFolder\\Result";
	//
	// pack(sourceFolder);
	// extract(packedFile, extractingFolder);
	// }
}
