package tcp.pop3uploaddownload.server;

import java.util.HashMap;

/**
 * Created by Eclipse on 30 January, 2018 9:10:20 PM.
 *
 * @author Joseph Maria
 *
 */
public class Data {
	private static HashMap<String, String> userData = new HashMap<String, String>();

	static {
		userData.put("TOMOE", "Truongem741");
		userData.put("YUNA", "Truongem852");
		userData.put("TOMOYO", "Truongem963");
	}

	public static boolean checkPassAndUser(String user, String pass) {
		return userData.containsKey(user.toUpperCase()) && userData.get(user.toUpperCase()).equals(pass);
	}

	public static boolean checkUser(String user) {
		return userData.containsKey(user.toUpperCase());
	}

}
