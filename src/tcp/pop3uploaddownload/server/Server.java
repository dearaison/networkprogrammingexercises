package tcp.pop3uploaddownload.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Eclipse on 30 January, 2018 8:44:32 PM.
 *
 * @author Joseph Maria
 *
 */
public class Server {
	public static void main(String[] args) throws IOException {
		@SuppressWarnings("resource")
		ServerSocket serverSocket = new ServerSocket(12345);
		System.out.println("Server is running");
		while (true) {
			Socket socket = serverSocket.accept();
			new Thread(new ServerProcessing(socket)).run();
		}
	}
}
