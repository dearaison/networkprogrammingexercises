package tcp.pop3uploaddownload.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * Created by Eclipse on 30 January, 2018 9:02:54 PM.
 *
 * @author Joseph Maria
 *
 */
public class ServerProcessing implements Runnable {
	private final DataOutputStream NET_OUT;
	private final DataInputStream NET_IN;
	private final Socket SOCKET;

	private String user = null;
	private boolean login = false;

	private String instruction = "";
	private String param;

	/**
	 * @param socket
	 * @throws IOException
	 */
	public ServerProcessing(Socket socket) throws IOException {
		this.SOCKET = socket;
		this.NET_OUT = new DataOutputStream(new BufferedOutputStream(this.SOCKET.getOutputStream()));
		this.NET_IN = new DataInputStream(new BufferedInputStream(this.SOCKET.getInputStream()));
	}

	/**
	 * @throws IOException
	 * 
	 */
	private void download() throws IOException {
		File file = new File(this.param);
		BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(file));

		NET_OUT.writeLong(file.length());
		NET_OUT.flush();

		int size;
		byte[] buffer = new byte[1024 * 1024];
		while ((size = fileIn.read(buffer)) > -1) {
			NET_OUT.write(buffer, 0, size);
		}
		NET_OUT.flush();
		fileIn.close();
	}

	/**
	 * @param command
	 * 
	 */
	private void processCommand(String command) {
		StringTokenizer tokenizer = new StringTokenizer(command, " ");
		if (tokenizer.countTokens() == 2) {
			this.instruction = tokenizer.nextToken().toUpperCase();
			this.param = tokenizer.nextToken();
		} else
			this.instruction = "";
	}

	/**
	 * 
	 */
	private String processPassCommand() {
		if (this.user == null) {
			return "Invalid login";
		}
		if (Data.checkPassAndUser(this.user, this.param)) {
			this.login = true;
			return "Pass is right";
		}
		return "Pass is wrong";
	}

	/**
	 * 
	 */
	private String processUserCommand() {
		if (Data.checkUser(this.param)) {
			this.user = this.param;
			return "User exist.";
		}
		return "User not exist.";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			NET_OUT.writeUTF("Welcome to File Management Service");
			NET_OUT.flush();

			while (!login) {
				String command = NET_IN.readUTF();

				if ("QUICK".equalsIgnoreCase(command)) {
					break;
				}

				processCommand(command);

				String respond = "Error.";
				switch (this.instruction) {
				case "USER":
					respond = processUserCommand();
					break;

				case "PASS":
					respond = processPassCommand();
					break;

				default:
					respond = "Command not found.";
					break;
				}
				NET_OUT.writeUTF(respond);
				NET_OUT.flush();

			}

			while (login) {
				String command = NET_IN.readUTF();

				if ("QUICK".equalsIgnoreCase(command)) {
					break;
				}

				processCommand(command);

				switch (this.instruction) {
				case "DOWNLOAD":
					download();
					break;

				case "UPLOAD":
					upload();
					break;

				default:
					break;
				}
			}
			this.SOCKET.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @throws IOException
	 * 
	 */
	private void upload() throws IOException {
		BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(this.param));

		long remain = NET_IN.readLong();
		byte[] buffer = new byte[1024 * 1024];
		int bytesNeedReading = (int) ((remain > buffer.length) ? buffer.length : remain);
		while (remain > 0) {
			int size = NET_IN.read(buffer, 0, bytesNeedReading);
			fileOut.write(buffer, 0, size);
			remain -= size;
			bytesNeedReading = (int) ((remain > buffer.length) ? buffer.length : remain);
		}
		fileOut.close();
	}
}
