package tcp.pop3uploaddownload.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

/**
 * Created by Eclipse on 31 January, 2018 10:15:12 AM.
 *
 * @author Joseph Maria
 *
 */
public class Client {
	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket socket = new Socket("127.0.0.1", 12345);
		new Client(socket).runClient();
		socket.close();
	}

	private final DataOutputStream NET_OUT;
	private final DataInputStream NET_IN;

	private final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));
	private String instruction = "";

	private String sf, df;

	/**
	 * @param host
	 * @param port
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public Client(Socket socket) throws UnknownHostException, IOException {
		super();
		this.NET_OUT = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		this.NET_IN = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
	}

	/**
	 * @throws IOException
	 * 
	 */
	private void download() throws IOException {
		BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(this.df));

		long remain = NET_IN.readLong();
		byte[] buffer = new byte[1024 * 1024];
		int bytesNeedReading = (int) ((remain > buffer.length) ? buffer.length : remain);
		while (remain > 0) {
			int size = NET_IN.read(buffer, 0, bytesNeedReading);
			fileOut.write(buffer, 0, size);
			remain -= size;
			bytesNeedReading = (int) ((remain > buffer.length) ? buffer.length : remain);
		}
		fileOut.close();
		System.out.println("Download successfully");
	}

	/**
	 * @param line
	 */
	private void processCommand(String line) {
		StringTokenizer tokenizer = new StringTokenizer(line, " ");
		if (tokenizer.countTokens() == 2) {
			this.instruction = tokenizer.nextToken().toUpperCase();
		} else if (tokenizer.countTokens() == 3) {
			this.instruction = tokenizer.nextToken().toUpperCase();
			this.sf = tokenizer.nextToken();
			this.df = tokenizer.nextToken();
		} else
			this.instruction = "";
	}

	/**
	 * @throws IOException
	 * 
	 */
	public void runClient() throws IOException {
		System.out.println(NET_IN.readUTF());
		while (true) {
			String line = READER.readLine();

			if ("QUICK".equalsIgnoreCase(line)) {
				NET_OUT.writeUTF(line);
				NET_OUT.flush();
				break;
			}

			processCommand(line);

			switch (this.instruction) {
			case "USER":
				NET_OUT.writeUTF(line);
				NET_OUT.flush();
				System.out.println(NET_IN.readUTF());
				break;

			case "PASS":
				NET_OUT.writeUTF(line);
				NET_OUT.flush();
				System.out.println(NET_IN.readUTF());
				break;

			case "DOWNLOAD":
				NET_OUT.writeUTF(this.instruction + " " + this.sf);
				NET_OUT.flush();
				download();
				break;

			case "UPLOAD":
				NET_OUT.writeUTF(this.instruction + " " + this.df);
				NET_OUT.flush();
				upload();
				break;

			default:
				System.out.println("Command not found!");
				break;
			}

		}
	}

	/**
	 * @throws IOException
	 * 
	 */
	private void upload() throws IOException {
		File file = new File(this.sf);
		BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(file));

		NET_OUT.writeLong(file.length());
		NET_OUT.flush();

		int size;
		byte[] buffer = new byte[1024 * 1024];
		while ((size = fileIn.read(buffer)) > -1) {
			NET_OUT.write(buffer, 0, size);
		}
		NET_OUT.flush();
		fileIn.close();
		System.out.println("Upload successfully");
	}

}
