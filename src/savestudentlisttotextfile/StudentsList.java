package savestudentlisttotextfile;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

public class StudentsList {
	public static final String UTF8 = "utf8";
	public static final String UTF16 = "utf16";

	public static void main(String[] args) throws IOException {
		String pathname = "B:\\baotruong11a\\TestFolder\\test - fileformat.txt";
		// String pathname2 = "D:\\15130210\\testcase\\Book1.txt";
		Subject subject = new Subject("Láº­p TrÃ¬nh NÃ¢ng Cao", 8.0);
		Subject subject2 = new Subject("Cáº¥u TrÃºc Dá»¯ Liá»‡u", 8.6);
		Subject subject3 = new Subject("Láº­p TrÃ¬nh Web", 8.7);

		Student student = new Student(15130210, "Neko", "14/2/1997");
		Student student2 = new Student(15130210, "Tomoyo", "14/2/1997");
		Student student3 = new Student(15130210, "Tomoe", "14/2/1997");

		student.addSubject(subject);
		student.addSubject(subject2);
		student.addSubject(subject3);

		student2.addSubject(subject);
		student2.addSubject(subject2);
		student2.addSubject(subject3);

		student3.addSubject(subject);
		student3.addSubject(subject2);
		student3.addSubject(subject3);

		StudentsList exporting = new StudentsList();
		exporting.addStudent(student);
		exporting.addStudent(student2);
		exporting.addStudent(student3);

		exporting.export(pathname);
		exporting.importStudents(pathname);
		System.out.print(exporting);

	}

	private List<Student> students;

	public StudentsList() {
		this.students = new ArrayList<Student>();
	}

	public void addStudent(Student student) {
		this.students.add(student);
	}

	public void export(String pathname) throws IOException {
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(pathname), UTF8));
		for (Student student : students) {
			printWriter.println(student.exportStudent());
		}
		printWriter.flush();
		printWriter.close();
	}

	public void importStudents(String pathname) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(pathname), UTF8));
		String line;
		while ((line = reader.readLine()) != null) {
			StringTokenizer tokenizer = new StringTokenizer(line, "\t");
			Student student = new Student();
			try {
				student.importStudent(tokenizer);
			} catch (ParseException e) {
				JOptionPane.showMessageDialog(null, "Birth date in the file: \"" + pathname + "\" are wrong format.",
						"Invalid date format", JOptionPane.ERROR_MESSAGE);
			}
			this.students.add(student);
		}
		reader.close();
	}

	// public void importStudents2(String studentFilePathname, String
	// subjectsFilePathname) throws IOException {
	// BufferedReader studentsReader = new BufferedReader(
	// new InputStreamReader(new FileInputStream(studentFilePathname), UTF8));
	// BufferedReader subjectsReader = new BufferedReader(
	// new InputStreamReader(new FileInputStream(subjectsFilePathname), UTF8));
	// String studenLine;
	// while ((studenLine = studentsReader.readLine()) != null) {
	// StringTokenizer tokenizer = new StringTokenizer(studenLine, "\t");
	// Student student = new Student();
	// this.students.add(student);
	// }
	//
	// List<String> subject;
	//
	// studentsReader.close();
	// subjectsReader.close();
	// }

	@Override
	public String toString() {
		String res = "";
		for (Student student : students) {
			res += student.toString() + "\n";
		}
		return res;
	}
}
