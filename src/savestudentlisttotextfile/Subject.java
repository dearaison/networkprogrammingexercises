package savestudentlisttotextfile;

import java.util.StringTokenizer;

public class Subject {
	private String name;
	private float score;

	public Subject() {
	}

	public Subject(String name, double score) {
		super();
		this.name = name;
		this.score = (float) score;

	}

	public String exportSubject() {
		return this.name + "\t" + this.score + "\t";
	}

	public void importSubject(StringTokenizer stringTokenizer) {
		this.name = stringTokenizer.nextToken();
		this.score = Float.parseFloat(stringTokenizer.nextToken());
	}

	@Override
	public String toString() {
		return this.name + "\t" + this.score + "\n";
	}
}
