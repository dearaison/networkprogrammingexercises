package savestudentlisttotextfile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

public class Student {
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
	private int ID;
	private String name;
	private Date birthDate;
	private List<Subject> subjects;

	public Student() {
		this.subjects = new ArrayList<>();
	}

	public Student(int iD, String name, String birthDate) {
		super();
		ID = iD;
		this.name = name;
		try {
			this.birthDate = dateFormat.parse(birthDate);
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, "You must enter the date of birth following DD/MM/YYYY format.",
					"Invalid date format", JOptionPane.ERROR_MESSAGE);
		}
		this.subjects = new ArrayList<Subject>();
	}

	public void addSubject(Subject subject) {
		this.subjects.add(subject);
	}

	public String exportStudent() {
		String result = "";
		result += this.ID + "\t";
		result += this.name + "\t";
		result += dateFormat.format(this.birthDate) + "\t";
		for (Subject subject : subjects) {
			result += subject.exportSubject();
		}
		return result;
	}

	public void importStudent(StringTokenizer stringTokenizer) throws ParseException {
		this.ID = Integer.parseInt(stringTokenizer.nextToken());
		this.name = stringTokenizer.nextToken();
		this.birthDate = dateFormat.parse(stringTokenizer.nextToken());
		while (stringTokenizer.hasMoreTokens()) {
			Subject subject = new Subject();
			subject.importSubject(stringTokenizer);
			this.subjects.add(subject);
		}
	}

	public boolean isMe(int iD) {
		return this.ID == iD;
	}

	@Override
	public String toString() {
		String result = "";
		result += this.ID + "\t";
		result += this.name + "\t";
		result += dateFormat.format(this.birthDate) + "\n";
		for (Subject subject : subjects) {
			result += subject.toString();
		}
		result += System.lineSeparator();
		return result;
	}
}
