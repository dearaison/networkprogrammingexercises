package studenttcpbinary;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Server implements Runnable {
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(12345);
		System.out.println("Server is running!");
		while (true) {
			Server server = new Server(serverSocket.accept());
			new Thread(server).start();
			System.out.println("Client is connected!");
		}
	}
	private Socket socket;
	private DataOutputStream out;

	private DataInputStream in;

	HashMap<String, Student> students = new HashMap<String, Student>();

	public Server(Socket socket) throws IOException {
		this.socket = socket;
		this.in = new DataInputStream(new BufferedInputStream(this.socket.getInputStream()));
		this.out = new DataOutputStream(new BufferedOutputStream(this.socket.getOutputStream()));
	}

	@Override
	public void run() {
		List<String> data = new LinkedList<String>();
		data.add("Client co the thao tao voi cac lech sau day: ");
		data.add("THEM_MSV_HOVATEN_SDT_DIEMMONHOC");
		data.add("SUA_MSV_HOVATEN_SDT_DIEMMONHOC");
		data.add("XOA_MSV");
		data.add("XEM_MSV");
		data.add("XEMTATCA");
		try {
			out.writeUTF("Welcome to student management service.");
			out.flush();
			while (true) {
				writeML(data);
				String line = in.readUTF();
				StringTokenizer token = new StringTokenizer(line, "_");
				if (token.countTokens() == 0) {
					out.writeUTF("Sai cu phap.");
					out.flush();
					continue;
				}
				String comand = token.nextToken();
				switch (comand) {
				case "THEM":
					them(line);
					break;
				case "XOA":
					xoa();
					break;
				case "SUA":
					sua();
					break;
				case "XEM":
					xem();
					break;
				case "XEMTATCA":
					xemtatca();
					break;

				default:
					out.writeUTF("Sai cu phap.");
					out.flush();
					continue;
				}
				this.socket.close();
			}
		} catch (Exception e) {

		}
	}

	private void sua() {
		// TODO Auto-generated method stub

	}

	private void them(String line) throws IOException {
		while (true) {
			StringTokenizer token = new StringTokenizer(line, "_");
			token.nextToken();
			them(token.nextToken(), token.nextToken(), token.nextToken(), token.nextToken());
			out.writeUTF("Ban co muon them tiep sv nua khong (Y/N)?");
			out.flush();
			line = in.readUTF();
			out.writeUTF("Moi nhap sv");
			out.flush();
			if ("Y".equals(line)) {
				line = in.readUTF();
				token = new StringTokenizer(line, "_");
				if (token.countTokens() < 0 || !"THEM".equals(token.nextToken())) {
					out.writeUTF("Sai cu phap.");
					out.flush();
					return;
				} else {
					continue;
				}
			} else {
				return;
			}
		}
	}

	private void them(String msv, String name, String phone, String score) throws IOException {
		try {
			students.put(msv, new Student(msv, name, phone, Double.parseDouble(score)));
			out.writeUTF("Them sinh vien ok.");
			out.flush();
		} catch (NumberFormatException e) {
			out.writeUTF("Diem khong hop le.");
			out.flush();
		}
	}

	public void writeML(List<String> list) throws IOException {
		for (String string : list) {
			System.out.println(string);
			out.writeUTF(string);
			out.flush();
		}
		out.writeUTF(".");
		out.flush();
	}

	private void xem() {
		// TODO Auto-generated method stub

	}

	private void xemtatca() throws IOException {
		List<String> list = new LinkedList<String>();
		for (Student sv : students.values()) {
			list.add(sv.toString());
		}
		writeML(list);
	}

	private void xoa() {
		// TODO Auto-generated method stub

	}

}
