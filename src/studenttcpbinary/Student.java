package studenttcpbinary;

public class Student {
	String iD;
	String name;
	String phone;
	double score;

	public Student(String msv, String name, String phone, double score) {
		super();
		this.iD = msv;
		this.name = name;
		this.phone = phone;
		this.score = score;
	}

	public String getMsv() {
		return iD;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public double getScore() {
		return score;
	}

	public void setMsv(String msv) {
		this.iD = msv;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return name + "\t" + iD + "\t" + phone + "\t" + score;
	}

}
