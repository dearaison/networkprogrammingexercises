package studenttcpbinary;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;

public class Client {
	public static void main(String[] args) throws IOException {
		new Client();
	}
	private Socket socket;
	private DataInputStream in;
	private DataOutputStream out;
	String ip = "localhost";
	int port = 12345;

	BufferedReader s = new BufferedReader(new InputStreamReader(System.in));

	public Client() throws IOException {
		this.socket = new Socket(ip, port);
		this.in = new DataInputStream(new BufferedInputStream(this.socket.getInputStream()));
		this.out = new DataOutputStream(new BufferedOutputStream(this.socket.getOutputStream()));

		System.out.println(in.readUTF());
		while (true) {
			readML();
			String line = s.readLine();
			out.writeUTF(line);
			out.flush();
			StringTokenizer token = new StringTokenizer(line, "_");
			if (token.countTokens() == 1 && "XEMTATCA".equals(token.nextToken())) {
				readML();
			} else
				System.out.println(in.readUTF());
		}
	}

	public void readML() throws IOException {
		String line;
		while (true) {
			line = in.readUTF();
			if (".".equals(line))
				break;
			System.out.println(line);
		}
	}
}
