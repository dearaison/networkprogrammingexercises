package savestudentlisttobinaryfile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class StudentsList {
	public static List<Student> loadList(String pathname) {
		ArrayList<Student> list = new ArrayList<Student>();
		try {
			DataInputStream IOS = new DataInputStream(new BufferedInputStream(new FileInputStream(pathname)));
			int listSize = IOS.readInt();
			for (int i = 0; i < listSize; i++) {
				Student student = new Student();
				student.load(IOS);
				list.add(student);
			}
			IOS.close();
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, "Birth date in the file: \"" + pathname + "\" are wrong format.",
					"Invalid date format", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Can read from: \"" + pathname + "\"  file.", "System Error",
					JOptionPane.ERROR_MESSAGE);
		}
		return list;
	}

	public static Student loadList(String pathname, int recordNumber) {
		DataInputStream IOS;
		try {
			IOS = new DataInputStream(new BufferedInputStream(new FileInputStream(pathname)));
			int listSize = IOS.readInt();
			if (recordNumber >= listSize) {
				IOS.close();
				return null;
			}
			Student student = new Student();
			for (int i = 0; i < recordNumber; i++) {
				student.load(IOS);
			}
			student.load(IOS);
			IOS.close();
			return student;
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, "Birth date in the file: \"" + pathname + "\" are wrong format.",
					"Invalid date format", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Can read from: \"" + pathname + "\"  file.", "System Error",
					JOptionPane.ERROR_MESSAGE);
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		String pathname = "B:\\baotruong11a\\TestFolder\\test - fileformat.txt";
		ArrayList<Student> list = new ArrayList<Student>();

		Subject subject = new Subject("Lập Trình Nâng Cao", 8.0);
		Subject subject2 = new Subject("Cấu Trúc Dữ Liệu", 8.6);
		Subject subject3 = new Subject("Lập Trình Web", 8.7);

		Student student1 = new Student(1, "Tomoe Kobayashi", "14/2/1997");
		Student student2 = new Student(2, "Tomoyo Watanabe", "24/12/2007");
		Student student3 = new Student(3, "Kanna Mikochi", "1/6/2010");

		student1.addSubject(subject);
		student1.addSubject(subject2);
		student1.addSubject(subject3);

		student2.addSubject(subject);
		student2.addSubject(subject2);
		student2.addSubject(subject3);

		student3.addSubject(subject);
		student3.addSubject(subject2);
		student3.addSubject(subject3);

		list.add(student1);
		list.add(student2);
		list.add(student3);
		
		saveList(list, pathname);
		ArrayList<Student> arrayList = (ArrayList<Student>) loadList(pathname);
		for (Student student : arrayList) {
			System.out.println(student.toString());
		}
		System.out.println(loadList(pathname, 0));
	}

	public static void saveList(List<Student> list, String pathname) {
		DataOutputStream DOS;
		try {
			DOS = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(pathname)));
			DOS.writeInt(list.size());
			for (Student student : list) {
				student.save(DOS);
			}
			DOS.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "System Error", "Can write into: \"" + pathname + "\"  file.",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
