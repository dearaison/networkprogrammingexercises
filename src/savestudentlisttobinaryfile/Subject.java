package savestudentlisttobinaryfile;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Subject {
	private String name;
	private float score;

	public Subject() {
		// TODO Auto-generated constructor stub
	}

	public Subject(String name, double score) {
		super();
		this.name = name;
		this.score = (float) score;

	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the score
	 */
	public float getScore() {
		return score;
	}

	public void load(DataInput dataInput) throws IOException {
		this.name = dataInput.readUTF();
		this.score = dataInput.readFloat();
	}

	public void save(DataOutput dataOutput) throws IOException {
		dataOutput.writeUTF(this.name);
		dataOutput.writeFloat(this.score);
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(float score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return this.name + "\t" + this.score + "\n";
	}
}
