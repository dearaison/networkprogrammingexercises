package savestudentlisttobinaryfile;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

public class Student {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
    private int ID;
    private String name;
    private Date birthDate;
    private List<Subject> subjects;

    public Student() {
        this.subjects = new ArrayList<>();
    }

    public Student(int iD, String name, String birthDate) {
        super();
        this.ID = iD;
        this.name = name;
        try {
            this.birthDate = dateFormat.parse(birthDate);
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, "You must enter the date of birth following DD/MM/YYYY format.",
                    "Invalid date format", JOptionPane.ERROR_MESSAGE);
        }
        this.subjects = new ArrayList<>();
    }

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
    }

    /**
     * @return the birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @return the iD
     */
    public int getID() {
        return ID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the subjects
     */
    public List<Subject> getSubjects() {
        return subjects;
    }

    public void load(DataInput dataInput) throws IOException, ParseException {
        this.ID = dataInput.readInt();
        this.name = dataInput.readUTF();
        this.birthDate = dateFormat.parse(dataInput.readUTF());
        int numberOfSubject = dataInput.readInt();
        for (int i = 0; i < numberOfSubject; i++) {
            Subject subject = new Subject();
            subject.load(dataInput);
            this.subjects.add(subject);
        }
    }

    public void save(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(this.ID);
        dataOutput.writeUTF(this.name);
        dataOutput.writeUTF(dateFormat.format(this.birthDate));
        dataOutput.writeInt(this.subjects.size());
        for (Subject subject : subjects) {
            subject.save(dataOutput);
        }
    }

    /**
     * @param birthDate the birthDate to set
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @param subjects the subjects to set
     */
    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        String result = this.ID + " " + this.name + " " + dateFormat.format(this.birthDate) + "\n";
        for (Subject subject : subjects) {
            result += subject.toString();
        }
        return result;
    }

}
