package networkcalculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.FileSystemNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerProcess implements Runnable {
	public static void main(String[] args) throws IOException {
		@SuppressWarnings("resource")
		ServerSocket serverSocket = new ServerSocket(12345);
		while (true) {
			Socket socket = serverSocket.accept();
			new Thread(new ServerProcess(socket)).start();
		}
	}
	private Socket socket;
	private BufferedReader netIn;
	private PrintWriter netOut;
	private char operator;

	private double operand1, operand2;

	/**
	 * @param socket
	 */
	public ServerProcess(Socket socket) {
		super();
		this.socket = socket;
	}

	private void analyzeRequest(String request) throws FileSystemNotFoundException {
		final Pattern COMMAND_PATTERN = Pattern.compile("-*\\d+[*+-/]-*\\d+");
		final Pattern OPERAND_PATTERN = Pattern.compile("-*\\d+");

		if (!COMMAND_PATTERN.matcher(request).matches()) {
			throw new FileSystemNotFoundException("Wrong command syntax!");
		} else {
			Matcher matcher = OPERAND_PATTERN.matcher(request);
			if (!matcher.find()) {
				throw new FileSystemNotFoundException("Operand must be a number");
			} else {
				String num1 = matcher.group();
				char opera = request.charAt(num1.length());
				String num2 = request.substring(request.indexOf(opera) + 1);

				try {
					operand1 = Double.parseDouble(num1.trim());
				} catch (NumberFormatException e) {
					operand1 = handleMultipleNegativeOperand(num1.trim());
				}

				operator = opera;

				try {
					operand2 = Double.parseDouble(num2.trim());
				} catch (NumberFormatException e) {
					operand2 = handleMultipleNegativeOperand(num2.trim());
				}
			}
		}
	}

	private double calculate() {
		double result = 0;
		switch (operator) {
		case '+':
			result = operand1 + operand2;
			break;

		case '-':
			result = operand1 - operand2;
			break;

		case '*':
			result = operand1 * operand2;
			break;

		case '/':
			result = operand1 / operand2;
			break;

		default:
			throw new FileSystemNotFoundException("Command not found");
		}
		return result;
	}

	private double handleMultipleNegativeOperand(String num) {
		String result;
		int numberOfNegativeSign = num.lastIndexOf('-') + 1;
		if (numberOfNegativeSign % 2 != 0) {
			result = "-" + num.substring(numberOfNegativeSign);
		} else {
			result = num.substring(numberOfNegativeSign);
		}
		return Double.parseDouble(result);
	}

	@Override
	public void run() {
		try {
			netIn = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			netOut = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream()), true);
			netOut.println("Ready!!!");

			while (true) {
				try {
					// get request
					String request = netIn.readLine();

					if ("exit".equalsIgnoreCase(request)) {
						break;
					}

					// Analyze request
					analyzeRequest(request);

					// Process request
					double result = calculate();
					netOut.println(request + " = " + result);
				} catch (FileSystemNotFoundException e) {
					netOut.println(e.getMessage());
				}
			}
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
