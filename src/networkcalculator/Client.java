package networkcalculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
	public static void main(String[] args) throws IOException {
		@SuppressWarnings("resource")
		Socket socket = new Socket("127.0.0.1", 12345);
		BufferedReader netIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter netOUt = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
		BufferedReader usserInput = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(netIn.readLine());
		while (true) {
			netOUt.println(usserInput.readLine());
			System.out.println(netIn.readLine());
		}
	}
}
